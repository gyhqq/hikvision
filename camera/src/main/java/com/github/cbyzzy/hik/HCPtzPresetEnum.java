package com.github.cbyzzy.hik;

public enum HCPtzPresetEnum {

    SET_PRESET(HCNetSDK.SET_PRESET, "设置预置点"),
    CLE_PRESET(HCNetSDK.CLE_PRESET, "清除预置点"),
    GOTO_PRESET(HCNetSDK.GOTO_PRESET, "快球转到预置点");

    private Integer code;
    private String msg;

    private  Integer value;

    private HCPtzPresetEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
